FROM python:3.12.0-slim as requirements-step

ENV POETRY_HOME=/tmp/poetry
ENV POETRY_VERSION=1.4.2
ENV PATH="${PATH}:$POETRY_HOME/bin"

RUN apt-get update > /dev/null  \
    && apt-get install -y curl > /dev/null
RUN curl -sSL https://install.python-poetry.org | python3 -

WORKDIR /tmp

COPY ./pyproject.toml /tmp/
COPY ./poetry.lock /tmp/

RUN mkdir -p requirements

RUN poetry export -f requirements.txt --output requirements/main.txt --only main
RUN poetry export -f requirements.txt --output requirements/dev.txt --only dev


FROM python:3.12.0-slim as main

ENV LC_ALL=C.UTF-8 \
    LANG=C.UTF-8 \
    PYTHONUNBUFFERED=1

WORKDIR /opt/app

COPY --from=requirements-step /tmp/requirements /opt/requirements
RUN pip install --no-cache-dir --upgrade -r /opt/requirements/main.txt

ADD ./src /opt/app/src

FROM main as dev

ADD ./tests /opt/app/tests

RUN pip install --no-cache-dir --upgrade -r /opt/requirements/dev.txt


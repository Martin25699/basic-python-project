DOCKER_DEV_COMPOSE ?= compose.dev.yml

# run autoformat
.PHONY: dev/autoformat
.ONESHELL:
dev/autoformat:
	@docker-compose -f $(DOCKER_DEV_COMPOSE) run --rm app /bin/bash -c \
		"autoflake --recursive --remove-all-unused-imports --remove-unused-variables --in-place src tests"
	@docker-compose -f $(DOCKER_DEV_COMPOSE) run --rm app /bin/bash -c \
		"black src tests"
	@docker-compose -f $(DOCKER_DEV_COMPOSE) run --rm app /bin/bash -c \
		"isort src tests"
